This is an installation and configuration Guide for a Debian Vulnerability Scanner (DVS)

Install & Configuration Index:
1. debian 12
2. timeshift
3. openSSH
4. automatic updates
5. Greenbone Community Edition
6. openSCAP
