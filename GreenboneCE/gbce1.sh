#!/bin/bash

# Source from January 30, 2024 and from the Website: https://greenbone.github.io/docs/latest/22.4/source-build/index.html

# Creating a gvm system user and group
(
  sudo useradd -r -M -U -G sudo -s /usr/sbin/nologin gvm
)
if [ $? -ne 0 ]; then
  echo "ERROR: Creating a gvm system user and group"
  exit 1
fi

# Add current user to gvm group
(
  sudo usermod -aG gvm $USER;
  su $USER
)
if [ $? -ne 0 ]; then
  echo "ERROR: Add current user to gvm group"
  exit 1
fi

# Setting an install prefix environment variable
(
  export INSTALL_PREFIX=/usr/local
)
if [ $? -ne 0 ]; then
  echo "ERROR: Setting an install prefix environment variable"
  exit 1
fi

# Adjusting PATH for running gvmd
(
  export PATH=$PATH:$INSTALL_PREFIX/sbin
)
if [ $? -ne 0 ]; then
  echo "ERROR: Adjusting PATH for running gvmd"
  exit 1
fi

# Choosing a source directory
(
  export SOURCE_DIR=$HOME/source
  mkdir -p $SOURCE_DIR
)
if [ $? -ne 0 ]; then
  echo "ERROR: Choosing a source directory"
  exit 1
fi

# Choosing a build directory
(
  export BUILD_DIR=$HOME/build
  mkdir -p $BUILD_DIR
)
if [ $? -ne 0 ]; then
  echo "ERROR: Choosing a build directory"
  exit 1
fi

# Choosing a temporary install directory
(
  export INSTALL_DIR=$HOME/install
  mkdir -p $INSTALL_DIR
)
if [ $? -ne 0 ]; then
  echo "ERROR: Choosing a temporary install directory"
  exit 1
fi

# Installing common build dependencies
(
  sudo apt update
  sudo apt install --no-install-recommends --assume-yes \
    build-essential \
    curl \
    cmake \
    pkg-config \
    python3 \
    python3-pip \
    gnupg
)
if [ $? -ne 0 ]; then
  echo "ERROR: Installing common build dependencies"
  exit 1
fi

# Importing the Greenbone Community Signing key
(
  curl -f -L https://www.greenbone.net/GBCommunitySigningKey.asc -o /tmp/GBCommunitySigningKey.asc
  gpg --import /tmp/GBCommunitySigningKey.asc
)
if [ $? -ne 0 ]; then
  echo "ERROR: Importing the Greenbone Community Signing key"
  exit 1
fi

# Setting the trust level for the Greenbone Community Signing key
(
  echo "8AE4BE429B60A59B311C2E739823FAA60ED1E580:6:" | gpg --import-ownertrust
)
if [ $? -ne 0 ]; then
  echo "ERROR: Setting the trust level for the Greenbone Community Signing key"
  exit 1
fi

# Required dependencies for gvm-libs
(
  sudo apt install -y \
    libglib2.0-dev \
    libgpgme-dev \
    libgnutls28-dev \
    uuid-dev \
    libssh-gcrypt-dev \
    libhiredis-dev \
    libxml2-dev \
    libpcap-dev \
    libnet1-dev \
    libpaho-mqtt-dev
)
if [ $? -ne 0 ]; then
  echo "ERROR: Required dependencies for gvm-libs"
  exit 1
fi

# Optional dependencies for gvm-libs
(
  sudo apt install -y \
    libldap2-dev \
    libradcli-dev
)
if [ $? -ne 0 ]; then
  echo "ERROR: Optional dependencies for gvm-libs"
  exit 1
fi

# Downloading the gvm-libs sources
(
  curl -f -L https://github.com/greenbone/gvm-libs/archive/refs/tags/v$GVM_LIBS_VERSION.tar.gz -o $SOURCE_DIR/gvm-libs-$GVM_LIBS_VERSION.tar.gz
  curl -f -L https://github.com/greenbone/gvm-libs/releases/download/v$GVM_LIBS_VERSION/gvm-libs-v$GVM_LIBS_VERSION.tar.gz.asc -o $SOURCE_DIR/gvm-libs-$GVM_LIBS_VERSION.tar.gz.asc
)
if [ $? -ne 0 ]; then
  echo "ERROR: Downloading the gvm-libs sources"
  exit 1
fi

# Verifying the 1. source file
(
  gpg --verify $SOURCE_DIR/gvm-libs-$GVM_LIBS_VERSION.tar.gz.asc $SOURCE_DIR/gvm-libs-$GVM_LIBS_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Verifying the 1. source file"
  exit 1
else
  echo "The output of the last command should be similar to:"
  echo "gpg: Signature made Fri Apr 16 08:31:02 2021 UTC"
  echo "gpg: using RSA key 9823FAA60ED1E580"
  echo "gpg: Good signature from Greenbone Community Feed integrity key [ultimate]"
fi

# Extracting 1. tarball
(
  tar -C $SOURCE_DIR -xvzf $SOURCE_DIR/gvm-libs-$GVM_LIBS_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Extracting 1. tarball"
  exit 1
fi

# Building gvm-libs
(
  mkdir -p $BUILD_DIR/gvm-libs && cd $BUILD_DIR/gvm-libs
  cmake $SOURCE_DIR/gvm-libs-$GVM_LIBS_VERSION \
    -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DCMAKE_BUILD_TYPE=Release \
    -DSYSCONFDIR=/etc \
    -DLOCALSTATEDIR=/var
  make -j$(nproc)
)
if [ $? -ne 0 ]; then
  echo "ERROR: Building gvm-libs"
  exit 1
fi

# Installing gvm-lib
(
  mkdir -p $INSTALL_DIR/gvm-libs
  make DESTDIR=$INSTALL_DIR/gvm-libs install
  sudo cp -rv $INSTALL_DIR/gvm-libs/* /
)
if [ $? -ne 0 ]; then
  echo "ERROR: Installing gvm-lib"
  exit 1
fi

# Setting the gvmd version to use
(
  export GVMD_VERSION=23.2.0
)
if [ $? -ne 0 ]; then
  echo "ERROR: Setting the gvmd version to use"
  exit 1
fi

# Required dependencies for gvmd
(
  sudo apt install -y \
    libglib2.0-dev \
    libgnutls28-dev \
    libpq-dev \
    postgresql-server-dev-15 \
    libical-dev \
    xsltproc \
    rsync \
    libbsd-dev \
    libgpgme-dev
)
if [ $? -ne 0 ]; then
  echo "ERROR: Required dependencies for gvmd"
  exit 1
fi

# Optional dependencies for gvmd
(
  sudo apt install -y --no-install-recommends \
    texlive-latex-extra \
    texlive-fonts-recommended \
    xmlstarlet \
    zip \
    rpm \
    fakeroot \
    dpkg \
    nsis \
    gnupg \
    gpgsm \
    wget \
    sshpass \
    openssh-client \
    socat \
    snmp \
    python3 \
    smbclient \
    python3-lxml \
    gnutls-bin \
    xml-twig-tools
)
if [ $? -ne 0 ]; then
  echo "ERROR: Optional dependencies for gvmd"
  exit 1
fi

# Downloading the gvmd sources
(
  curl -f -L https://github.com/greenbone/gvmd/archive/refs/tags/v$GVMD_VERSION.tar.gz -o $SOURCE_DIR/gvmd-$GVMD_VERSION.tar.gz
  curl -f -L https://github.com/greenbone/gvmd/releases/download/v$GVMD_VERSION/gvmd-$GVMD_VERSION.tar.gz.asc -o $SOURCE_DIR/gvmd-$GVMD_VERSION.tar.gz.asc
)
if [ $? -ne 0 ]; then
  echo "ERROR: Downloading the gvmd sources"
  exit 1
fi

# Verifying the 2. source file
(
  gpg --verify $SOURCE_DIR/gvmd-$GVMD_VERSION.tar.gz.asc $SOURCE_DIR/gvmd-$GVMD_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Verifying the 2. source file"
  exit 1
else
  echo "The output of the last command should be similar to:"
  echo "gpg: Signature made Fri Apr 16 08:31:02 2021 UTC"
  echo "gpg: using RSA key 9823FAA60ED1E580"
  echo "gpg: Good signature from Greenbone Community Feed integrity key [ultimate]"
fi

# Extracting 2. tarball
(
  tar -C $SOURCE_DIR -xvzf $SOURCE_DIR/gvmd-$GVMD_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Extracting 2. tarball"
  exit 1
fi

# Building gvmd
(
  mkdir -p $BUILD_DIR/gvmd && cd $BUILD_DIR/gvmd
  cmake $SOURCE_DIR/gvmd-$GVMD_VERSION \
    -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DCMAKE_BUILD_TYPE=Release \
    -DLOCALSTATEDIR=/var \
    -DSYSCONFDIR=/etc \
    -DGVM_DATA_DIR=/var \
    -DGVMD_RUN_DIR=/run/gvmd \
    -DOPENVAS_DEFAULT_SOCKET=/run/ospd/ospd-openvas.sock \
    -DGVM_FEED_LOCK_PATH=/var/lib/gvm/feed-update.lock \
    -DSYSTEMD_SERVICE_DIR=/lib/systemd/system \
    -DLOGROTATE_DIR=/etc/logrotate.d
  make -j$(nproc)
)
if [ $? -ne 0 ]; then
  echo "ERROR: Building gvmd"
  exit 1
fi

# Installing gvmd
(
  mkdir -p $INSTALL_DIR/gvmd
  make DESTDIR=$INSTALL_DIR/gvmd install
  sudo cp -rv $INSTALL_DIR/gvmd/* /
)
if [ $? -ne 0 ]; then
  echo "ERROR: Installing gvmd"
  exit 1
fi

# Setting the pg-gvm version to use
(
  export PG_GVM_VERSION=22.6.4
)
if [ $? -ne 0 ]; then
  echo "ERROR: Setting the pg-gvm version to use"
  exit 1
fi

# Required dependencies for pg-gvm
(
  sudo apt install -y \
    libglib2.0-dev \
    postgresql-server-dev-15 \
    libical-dev
)
if [ $? -ne 0 ]; then
  echo "ERROR: Required dependencies for pg-gvm"
  exit 1
fi

# Downloading the pg-gvm sources
(
  curl -f -L https://github.com/greenbone/pg-gvm/archive/refs/tags/v$PG_GVM_VERSION.tar.gz -o $SOURCE_DIR/pg-gvm-$PG_GVM_VERSION.tar.gz
  curl -f -L https://github.com/greenbone/pg-gvm/releases/download/v$PG_GVM_VERSION/pg-gvm-$PG_GVM_VERSION.tar.gz.asc -o $SOURCE_DIR/pg-gvm-$PG_GVM_VERSION.tar.gz.asc
)
if [ $? -ne 0 ]; then
  echo "ERROR: Downloading the pg-gvm sources"
  exit 1
fi

# Verifying the 3. source file
(
  gpg --verify $SOURCE_DIR/pg-gvm-$PG_GVM_VERSION.tar.gz.asc $SOURCE_DIR/pg-gvm-$PG_GVM_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Verifying the 3. source file"
  exit 1
else
  echo "The output of the last command should be similar to:"
  echo "gpg: Signature made Fri Apr 16 08:31:02 2021 UTC"
  echo "gpg: using RSA key 9823FAA60ED1E580"
  echo "gpg: Good signature from Greenbone Community Feed integrity key [ultimate]"
fi

# Extracting 3. tarball
(
  tar -C $SOURCE_DIR -xvzf $SOURCE_DIR/pg-gvm-$PG_GVM_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Extracting 3. tarball"
  exit 1
fi

# Building pg-gvm
(
  mkdir -p $BUILD_DIR/pg-gvm && cd $BUILD_DIR/pg-gvm
  cmake $SOURCE_DIR/pg-gvm-$PG_GVM_VERSION \
    -DCMAKE_BUILD_TYPE=Release
  make -j$(nproc)
)
if [ $? -ne 0 ]; then
  echo "ERROR: Building pg-gvm"
  exit 1
fi

# Installing pg-gvm
(
  mkdir -p $INSTALL_DIR/pg-gvm
  make DESTDIR=$INSTALL_DIR/pg-gvm install
  sudo cp -rv $INSTALL_DIR/pg-gvm/* /
)
if [ $? -ne 0 ]; then
  echo "ERROR: Installing pg-gvm"
  exit 1
fi

# Setting the GSA version to us
(
  export GSA_VERSION=23.0.0
)
if [ $? -ne 0 ]; then
  echo "ERROR: Setting the GSA version to us"
  exit 1
fi

# Downloading the gsa sources
(
  curl -f -L https://github.com/greenbone/gsa/releases/download/v$GSA_VERSION/gsa-dist-$GSA_VERSION.tar.gz -o $SOURCE_DIR/gsa-$GSA_VERSION.tar.gz
  curl -f -L https://github.com/greenbone/gsa/releases/download/v$GSA_VERSION/gsa-dist-$GSA_VERSION.tar.gz.asc -o $SOURCE_DIR/gsa-$GSA_VERSION.tar.gz.asc
)
if [ $? -ne 0 ]; then
  echo "ERROR: Downloading the gsa sources"
  exit 1
fi

# Verifying the 4. source file
(
  gpg --verify $SOURCE_DIR/gsa-$GSA_VERSION.tar.gz.asc $SOURCE_DIR/gsa-$GSA_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Verifying the 4. source file"
  exit 1
else
  echo "The output of the last command should be similar to:"
  echo "gpg: Signature made Fri Apr 16 08:31:02 2021 UTC"
  echo "gpg: using RSA key 9823FAA60ED1E580"
  echo "gpg: Good signature from Greenbone Community Feed integrity key [ultimate]"
fi

# Extracting 4. tarball
(
  mkdir -p $SOURCE_DIR/gsa-$GSA_VERSION
  tar -C $SOURCE_DIR/gsa-$GSA_VERSION -xvzf $SOURCE_DIR/gsa-$GSA_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Extracting 4. tarball"
  exit 1
fi

# Installing gsa
(
  sudo mkdir -p $INSTALL_PREFIX/share/gvm/gsad/web/
  sudo cp -rv $SOURCE_DIR/gsa-$GSA_VERSION/* $INSTALL_PREFIX/share/gvm/gsad/web/
)
if [ $? -ne 0 ]; then
  echo "ERROR: Installing gsa"
  exit 1
fi

# Setting the GSAd version to use
(
  export GSAD_VERSION=22.9.0
)
if [ $? -ne 0 ]; then
  echo "ERROR: Setting the GSAd version to use"
  exit 1
fi

# Required dependencies for gsad
(
  sudo apt install -y \
    libmicrohttpd-dev \
    libxml2-dev \
    libglib2.0-dev \
    libgnutls28-dev
)
if [ $? -ne 0 ]; then
  echo "ERROR: Required dependencies for gsad"
  exit 1
fi

# Downloading the gsad sources
(
  curl -f -L https://github.com/greenbone/gsad/archive/refs/tags/v$GSAD_VERSION.tar.gz -o $SOURCE_DIR/gsad-$GSAD_VERSION.tar.gz
  curl -f -L https://github.com/greenbone/gsad/releases/download/v$GSAD_VERSION/gsad-$GSAD_VERSION.tar.gz.asc -o $SOURCE_DIR/gsad-$GSAD_VERSION.tar.gz.asc
)
if [ $? -ne 0 ]; then
  echo "ERROR: Downloading the gsad sources"
  exit 1
fi

# Verifying the 5. source file
(
  gpg --verify $SOURCE_DIR/gsad-$GSAD_VERSION.tar.gz.asc $SOURCE_DIR/gsad-$GSAD_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Verifying the 5. source file"
  exit 1
else
  echo "The output of the last command should be similar to:"
  echo "gpg: Signature made Fri Apr 16 08:31:02 2021 UTC"
  echo "gpg: using RSA key 9823FAA60ED1E580"
  echo "gpg: Good signature from Greenbone Community Feed integrity key [ultimate]"
fi

# Extracting 5. tarball
(
  tar -C $SOURCE_DIR -xvzf $SOURCE_DIR/gsad-$GSAD_VERSION.tar.gz
)
if [ $? -ne 0 ]; then
  echo "ERROR: Extracting 5. tarball"
  exit 1
fi

# Building gsad
(
  mkdir -p $BUILD_DIR/gsad && cd $BUILD_DIR/gsad
  cmake $SOURCE_DIR/gsad-$GSAD_VERSION \
    -DCMAKE_INSTALL_PREFIX=$INSTALL_PREFIX \
    -DCMAKE_BUILD_TYPE=Release \
    -DSYSCONFDIR=/etc \
    -DLOCALSTATEDIR=/var \
    -DGVMD_RUN_DIR=/run/gvmd \
    -DGSAD_RUN_DIR=/run/gsad \
    -DLOGROTATE_DIR=/etc/logrotate.d
  make -j$(nproc)
)
if [ $? -ne 0 ]; then
  echo "ERROR: Building gsad"
  exit 1
fi

# Installing gsad
(
  mkdir -p $INSTALL_DIR/gsad
  make DESTDIR=$INSTALL_DIR/gsad install
  sudo cp -rv $INSTALL_DIR/gsad/* /
)
if [ $? -ne 0 ]; then
  echo "ERROR: Installing gsad"
  exit 1
fi

#info: next step is openvas-smb
# Setting the openvas-smb version to use

